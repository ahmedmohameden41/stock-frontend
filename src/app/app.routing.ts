﻿import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {AuthGuard} from './helpers/auth.guard';
import {LoginComponent} from './pages/login/login.component';
import {InvoiceComponent} from './components/adherant/invoice.component';
import {ClientComponent} from './components/client/client.component';
import {ProductComponent} from './components/product/product.component';
import {StockComponent} from './components/stock/stock.component';
import {SupplierComponent} from './components/supplier/supplier.component';
import {OrderComponent} from './components/order/order.component';
import {AvoirComponent} from './components/avoir/avoir.component';
import {TransactionComponent} from './components/transaction/transaction.component';
import {CaseComponent} from './components/case/case.component';

const routes: Routes = [
  {path: '', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'adherants', component: InvoiceComponent},
  {path: 'transaction', component: TransactionComponent},
  {path: 'stock', component: StockComponent},
  {path: 'client', component: ClientComponent},
  {path: 'supplier', component: SupplierComponent},
  {path: 'order', component: OrderComponent},
  {path: 'case', component: CaseComponent},
  {path: 'avoir', component: AvoirComponent},
  {path: 'product', component: ProductComponent},

  // otherwise redirect to home
  {path: '**', redirectTo: ''},
];

export const appRoutingModule = RouterModule.forRoot(routes);
