﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {appRoutingModule} from './app.routing';
import {AppComponent} from './app.component';
import {HomeComponent} from './pages/home/home.component';
import {LoginComponent} from './pages/login/login.component';
import {AlertComponent} from './components/alert.component';
import {JwtInterceptor} from './helpers/jwt.interceptor';
import {ErrorInterceptor} from './helpers/error.interceptor';
import {InputOnlyNumberDirective} from './shared/input-only-number.directive';
import {AgGridModule} from 'ag-grid-angular';
import {AgGridActionsButtonsComponent} from './shared/ag-grid-actions-buttons/ag-grid-actions-buttons.component';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {RbacDirective} from './directive/rbac.directive';
import {DatePipe} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {UploadModalModule} from './shared/upload-modal/upload-modal.module';
import {MModalComponent} from './shared/m-modal/m-modal.component';
import {InvoiceComponent} from './components/adherant/invoice.component';
import {ClientComponent} from './components/client/client.component';
import {ProductComponent} from './components/product/product.component';
import {StockComponent} from './components/stock/stock.component';
import {SupplierComponent} from './components/supplier/supplier.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {OrderComponent} from './components/order/order.component';
import {OrderDetailComponent} from './components/order/order-detail/order-detail.component';
import {AvoirComponent} from './components/avoir/avoir.component';
import {DetailsInvoiceComponent} from './components/adherant/details-invoice/details-invoice.component';
import {MfDynamicFormModule} from 'mf-dynamic-form';
import {HeaderButtonsComponent} from './shared/list-header-buttons/header-buttons.component';
import {CategoryComponent} from './components/category/category.component';
import {FormModalComponent} from './shared/form-modal/form-modal.component';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {CreateOrderComponent} from './components/order/create-order/create-order.component';
import {CreateVoiceComponent} from './components/adherant/create-voice/create-voice.component';
import {MultiSelectModule} from 'primeng';
import {NgxPrintModule} from 'ngx-print';
import {TransactionComponent} from './components/transaction/transaction.component';
import {CreateTransactionComponent} from './components/transaction/create-transaction/create-transaction.component';;
import { DetailsClientComponent } from './components/client/details-client/details-client.component';
import { CaseComponent } from './components/case/case.component';
import { NewCaseComponent } from './components/case/new-case/new-case.component';;
import { LeftNavbarComponent } from './shared/left-navbar/left-navbar.component';
import { ActionBarItemComponent } from './shared/action-bar-item/action-bar-item.component'
;
import { NavbarComponent } from './shared/navbar/navbar.component'
// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);

}


@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    appRoutingModule,
    FormsModule,
    MfDynamicFormModule,
    FontAwesomeModule,
    AgGridModule.withComponents([AgGridActionsButtonsComponent]),
    NgbModule,
    NgxPrintModule,
    MultiSelectModule,
    AutoCompleteModule,
    TranslateModule.forRoot(
      {
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }
    ),
    UploadModalModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    AlertComponent,
    InputOnlyNumberDirective,
    AgGridActionsButtonsComponent,
    RbacDirective,
    MModalComponent,
    InvoiceComponent,
    ClientComponent,
    ProductComponent,
    StockComponent,
    SupplierComponent,
    OrderComponent,
    OrderDetailComponent,
    AvoirComponent,
    HeaderButtonsComponent,
    DetailsInvoiceComponent,
    CategoryComponent,
    FormModalComponent,
    CreateOrderComponent,
    CreateVoiceComponent,
    TransactionComponent,
    CreateTransactionComponent,
    DetailsClientComponent,
    CaseComponent,
    NewCaseComponent,
    LeftNavbarComponent
,
    ActionBarItemComponent ,
    NavbarComponent ],
  exports: [
    TranslateModule,
    MModalComponent
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    DatePipe
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
