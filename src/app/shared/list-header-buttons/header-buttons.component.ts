import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-header-buttons',
  templateUrl: './header-buttons.component.html',
  styleUrls: ['./header-buttons.component.scss']
})
export class HeaderButtonsComponent implements OnInit {
  @Input()
  buttons: any[];
  title: any;

  constructor() {
  }

  @Input()
  public set selectAll(value: boolean) {
    let checkAll = this.buttons.find(button => button.title === 'data-table.tooltips.checkAll');
    if (!!checkAll) {
      value ? checkAll.icon = "fa fa-check-square-o" : checkAll.icon = "fa fa-square-o";
    }
  }

  ngOnInit() {
  }
}
