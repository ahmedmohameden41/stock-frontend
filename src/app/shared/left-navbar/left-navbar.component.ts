import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-left-navbar',
  templateUrl: './left-navbar.component.html',
  styleUrls: ['./left-navbar.component.scss']
})
export class LeftNavbarComponent implements OnInit {

  expanded: boolean = false;
  @Output() expandedEvent = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit(): void {
  }

  expandedLeftNavBar() {
    this.expanded = !this.expanded;
    this.expandedEvent.emit(this.expanded);
  }
}
