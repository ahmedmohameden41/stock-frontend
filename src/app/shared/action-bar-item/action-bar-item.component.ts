import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-action-bar-item',
  templateUrl: './action-bar-item.component.html',
  styleUrls: ['./action-bar-item.component.scss']
})
export class ActionBarItemComponent implements OnInit {

  @Input() shadow: boolean = false;

  constructor() {
  }

  ngOnInit(): void {
  }

}
