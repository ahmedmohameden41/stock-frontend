import {Component, OnInit, ViewChild} from '@angular/core';
import {DynamicFormComponent, MfForm} from 'mf-dynamic-form';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss']
})
export class FormModalComponent implements OnInit {

  @ViewChild(DynamicFormComponent)
  dynamicForm: DynamicFormComponent;
  form: MfForm;

  i18n = {
    save: 'حفظ',
    errors: {
      isRequired: 'هذه الخانة مطلوبه',
      minLength: 'الحد الأدنى للطول هو',
      maxLength: 'الحد الأقصى للطول هو',
      emailInvalid: 'invalid', alphanumeric: 'doit être Alphanumeric',
      passwordMismatch: 'Les mots de passe ne sont pas identiques'
    }
  };

  constructor(private translate: TranslateService,
              public activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
  }

  triggerSubmit(event) {
    this.dynamicForm.submit();
  }

  isFormValid() {
    return this.dynamicForm?.isFormValid();
  }

  submit(model?: any) {
    if (!model) {
      return;
    }
    this.activeModal.close(model);
  }

}
