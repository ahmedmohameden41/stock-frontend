import {EventEmitter, Input, NgModule, Output} from "@angular/core";
import { CommonModule } from "@angular/common";
import { UploadModalComponent } from "./upload-modal.component";
import { TranslateModule } from "@ngx-translate/core";
import { NgbProgressbarModule } from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {RoleEnum} from "../../models/role-enum";
import {User} from "../../models/user";

@NgModule({
  declarations: [UploadModalComponent],
  imports: [CommonModule, TranslateModule, NgbProgressbarModule, FormsModule,
    ReactiveFormsModule]
})
export class UploadModalModule {}
