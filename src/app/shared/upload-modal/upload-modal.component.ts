import {Component, Input, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {OrderService} from "../../services/order.service";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

export class Adherant {
  nom: string;
  telephone: string;
  nni: string;
  id: number;
  cotiser: boolean;
  voter: boolean;
  public constructor() {
    this.nom = null;
    this.telephone = null;
    this.nni = null;
    this.id = null;
    this.cotiser = null;
    this.voter = null;
  }
};

export enum MODALS {
  CONFIRM = 10,
  DISMISS = 0
}


@Component({
  selector: "fta-upload-modal",
  templateUrl: "./upload-modal.component.html",
  styleUrls: ["./upload-modal.component.scss"]
})
export class UploadModalComponent implements OnInit {
  _adherant: Adherant = new Adherant();
  adherantForm: FormGroup;
  MODALS = MODALS;

  constructor(
    private formBuilder: FormBuilder,
    private AdherantService: OrderService,
    public modal: NgbActiveModal
  ) {
  }

  ngOnInit() {
    this.buildEditForm();
  }

  buildEditForm() {
    this.adherantForm = this.formBuilder.group({
      id: [this._adherant.id, Validators.required],
      nom: [this._adherant.nom, Validators.required],
      telephone: [this._adherant.telephone, Validators.required],
      nni: [this._adherant.nni, Validators.required],
      voter: [this._adherant.voter, Validators.required],
      cotiser: [this._adherant.cotiser, Validators.required],
    });
  }



  onSubmit() {
    const adherant = this.adherantForm.value;
    this.AdherantService.saveAdherant([adherant]).subscribe(elem => {
      this.modal.close(elem);
    });
  }

}
