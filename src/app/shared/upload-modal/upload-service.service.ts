import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class UploadServiceService {
  constructor(protected http: HttpClient) {}

  save(data: FormData): Observable<any> {
    return this.http.post<any>("/api/documents", data, {
      observe: "response",
      reportProgress: true
    });
  }

  download(filename: string): Observable<any> {
    return this.http.get("/downloadfile/" + filename, {
      observe: "response"
    });
  }
}
