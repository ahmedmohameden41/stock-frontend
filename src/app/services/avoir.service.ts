﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable({providedIn: 'root'})
export class AvoirService {
  constructor(private http: HttpClient) {
  }

  getAllAvoirs() {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/avoirs`);
  }

  saveAvoir(data: any) {
    return this.http.post<any>(`${environment.backendPrefix}/api/v1/avoirs`, data, {
      observe: 'response',
      reportProgress: true
    });
  }

}
