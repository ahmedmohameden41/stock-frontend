﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable({providedIn: 'root'})
export class TransactionService {
  constructor(private http: HttpClient) {
  }

  getAllTransactions() {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/transactions`);
  }

  saveTransaction(data: any) {
    return this.http.post<any>(`${environment.backendPrefix}/api/v1/transactions`, data, {
      observe: 'response',
      reportProgress: true
    });
  }

  deleteTransaction(id: number) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/transactions/delete-transaction/${id}`);
  }

  getAllTransactionByBusinesTypeAndBusinesId(busineType: string, busineId: number) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/transactions/by-busines-type-and-id//${busineType}/${busineId}`);
  }

}
