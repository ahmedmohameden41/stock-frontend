﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable({providedIn: 'root'})
export class InvoiceService {
  constructor(private http: HttpClient) {
  }

  getAllInvoices() {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/invoices`);
  }

  getAllOrderItemByOrderId(id) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/order-item/by-invoice/${id}`);
  }


  getAllByClientId(clientId: number) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/invoices/by-client-id//${clientId}`);
  }


  saveInvoices(data: any) {
    return this.http.post<any>(`${environment.backendPrefix}/api/v1/invoices`, data, {
      observe: 'response',
      reportProgress: true
    });
  }


  deleteInvoice(id: number) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/invoices/delete-invoice/` + id);
  }

}
