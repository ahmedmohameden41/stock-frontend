﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Client} from "../components/client/client.component";


@Injectable({providedIn: 'root'})
export class GroupeService {
  constructor(private http: HttpClient) {
  }

  getAllClients() {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/clients`);
  }

  saveAllGroupe(data: any[]) {
    return this.http.post<Client>(`${environment.backendPrefix}/api/v1/clients`, data, {
      observe: 'response',
      reportProgress: true
    });
  }
}
