﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ICategory} from '../components/product/product.component';


@Injectable({providedIn: 'root'})
export class CategoryService {
  constructor(private http: HttpClient) {
  }

  getAllCategories() {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/categories`);
  }

  saveAllCategories(data: any[]) {
    return this.http.post<ICategory>(`${environment.backendPrefix}/api/v1/categories`, data, {
      observe: 'response',
      reportProgress: true
    });
  }


  deleteCategory(id: number) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/categories/delete-category/` + id);
  }
}
