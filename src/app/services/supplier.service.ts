﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Supplier} from '../components/supplier/supplier.component';


@Injectable({providedIn: 'root'})
export class SupplierService {
  constructor(private http: HttpClient) {
  }

  getAllSuppliers() {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/supplier`);
  }

  saveAllSuppliers(data: any[]) {
    return this.http.post<Supplier>(`${environment.backendPrefix}/api/v1/supplier`, data, {
      observe: 'response',
      reportProgress: true
    });
  }
}
