﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Client} from '../components/client/client.component';


@Injectable({providedIn: 'root'})
export class ProductService {
  constructor(private http: HttpClient) {
  }

  getAllProducts() {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/products`);
  }

  deleteProduct(id: number) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/products/delete-product/` + id);
  }

  saveAllProducts(data: any[]) {
    return this.http.post<Client>(`${environment.backendPrefix}/api/v1/products`, data, {
      observe: 'response',
      reportProgress: true
    });
  }
}
