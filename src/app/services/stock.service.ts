﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ICategory} from '../components/product/product.component';


@Injectable({providedIn: 'root'})
export class StockService {
  constructor(private http: HttpClient) {
  }

  getAllStocks() {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/order-item/stocks`);
  }

}
