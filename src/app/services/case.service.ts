﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable({providedIn: 'root'})
export class CaseService {
  constructor(private http: HttpClient) {
  }

  getAllCases() {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/cases`);
  }

  saveCase(data: any) {
    return this.http.post<any>(`${environment.backendPrefix}/api/v1/cases`, data, {
      observe: 'response',
      reportProgress: true
    });
  }

  deleteCase(id: number) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/cases/delete-case/${id}`);
  }

}
