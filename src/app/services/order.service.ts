﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable({providedIn: 'root'})
export class OrderService {
  constructor(private http: HttpClient) {
  }

  getAllOrders() {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/orders`);
  }

  getAllOrderItemByOrderId(id) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/order-item/by-order/${id}`);
  }

  saveAdherant(data: any) {
    return this.http.post<any>(`${environment.backendPrefix}/api/v1/orders`, data, {
      observe: 'response',
      reportProgress: true
    });
  }


  getAllOrderBySupplierId(supplierId: number) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/orders/by-supplier-id//${supplierId}`);
  }

  deleteOrder(id: number) {
    return this.http.get<any[]>(`${environment.backendPrefix}/api/v1/orders/delete-order/` + id);
  }

}
