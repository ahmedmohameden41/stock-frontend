﻿import {Component, OnInit} from '@angular/core';
import {ColumnApi, GridApi, GridOptions} from 'ag-grid-community';
import {AgGridActionsButtonsComponent} from '../../shared/ag-grid-actions-buttons/ag-grid-actions-buttons.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UploadModalComponent} from '../../shared/upload-modal/upload-modal.component';
import {OrderService} from '../../services/order.service';
import {InvoiceService} from '../../services/invoice.service';
import {element} from 'protractor';
import {CaseService} from '../../services/case.service';

@Component({
  templateUrl: 'home.component.html',
  styleUrls: ['home.components.scss']
})
export class HomeComponent implements OnInit {
  factures: number = 0;
  detteFactures: number = 0;
  commandes: number = 0;
  detteCommandes: number = 0;

  receiptAmount: number = 0;
  receiptDevise: number = 0;
  expenditureAmount: number = 0;
  expenditureDevise: number = 0;

  constructor(private invoiceService: InvoiceService,
              private caseService: CaseService,
              private orderService: OrderService) {
  }

  private loadAdherants() {
    this.invoiceService.getAllInvoices().subscribe(invoices => {
      invoices.forEach(elem => {
        this.factures = this.factures + elem.totalAmount;
        this.detteFactures = this.detteFactures + (elem.totalAmount - elem.partialAmount);
      });
    });
  }

  private getAllOrder() {
    this.orderService.getAllOrders().subscribe(cmmd => {
      cmmd.filter(c => c.status !== 'COMMANDE').forEach(elem => {
        this.detteCommandes = this.detteCommandes + elem.totalAmount;
      });
    });
  }

  ngOnInit(): void {
    this.loadAdherants();
    this.getAllOrder();
    this.getAllCases();
  }

  private getAllCases() {
    this.caseService.getAllCases().subscribe(cs => {
      cs.forEach(elem => {
        if (elem.toMe) {
          this.receiptAmount = this.receiptAmount + elem.amount;
          this.receiptDevise = this.receiptDevise + elem.devise;
        } else {
          this.expenditureAmount = this.expenditureAmount + elem.amount;
          this.expenditureDevise = this.expenditureDevise + elem.devise;
        }
      });
    });
  }
}
