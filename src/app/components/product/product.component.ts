import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../../services/category.service';
import {FormBuilder} from '@angular/forms';
import {ProductService} from '../../services/products.service';
import {ColumnApi, GridApi, GridOptions} from 'ag-grid-community';
import {MfForm} from 'mf-dynamic-form';
import {FormParams} from '../../forms/category.-form';
import {FormModalComponent} from '../../shared/form-modal/form-modal.component';
import {TranslateService} from '@ngx-translate/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {getProductFormDefinition} from '../../forms/product-form';
import {cloneDeep} from 'lodash';

export interface IProduct {
  category: ICategory;
  id: number;
  label: string;
  note: string;
}

export interface ICategory {
  id: number;
  label: string;
}

export const DEFAULT_PRODUCT = {
  category: null,
  id: null,
  label: '',
  note: ''
};

export const DEFAULT_CATEGORY = {
  id: null,
  label: ''
};

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  gridOptions: GridOptions;
  frameworkComponents: any;
  rowData: any[] = [];
  gridApi: GridApi;
  columnApi: ColumnApi;
  buttons: any[] = [];

  form: MfForm;
  newClient: boolean = false;

  product: IProduct = {
    category: null, label: null, note: null, id: null
  };

  filteredProducts: IProduct[];
  products: IProduct [] = [];

  // I want to set default values
  category: any;
  categories: any[] = [];

  constructor(private productService: ProductService,
              private translate: TranslateService,
              private modalService: NgbModal,
              private categoryService: CategoryService, private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.getAllProducts();
    this.getAllCategories();

    this.initAgGrid();
    this.fetchData();
  }

  getAllProducts() {
    this.productService.getAllProducts().subscribe(res => {
      this.products = res;
      this.filteredProducts = this.products;
    });
  }

  getAllCategories() {
    this.categoryService.getAllCategories().subscribe(res => {
      this.categories = res;
    });
  }

  initAgGrid() {
    this.buttons = [
      {
        title: 'زبون جديد',
        icon: 'fa fa-plus-circle',
        fn: () => this.create(),
        disabled: () => {
          return !this.gridApi;
        }
      },
      {
        title: 'تعديل معلومات الزبون',
        icon: 'fa fa-edit',
        fn: () => this.create(this.gridApi.getSelectedRows()[0]),
        disabled: () => this.gridApi && this.gridApi.getSelectedRows().length !== 1
      },
      {
        title: 'حذف الزبون',
        icon: 'fa fa-trash',
        fn: () => this.deleteClient(this.gridApi.getSelectedRows()[0].id),
        disabled: () => (this.gridApi && this.gridApi.getSelectedRows().length !== 1),
      }
    ];
    this.gridOptions = <GridOptions> {
      pagination: false,
      rowModelType: 'clientSide',
      enableColResize: true,
      enableSorting: true,
      enableFilter: true,
      defaultColDef: {
        resizable: true,
        flex: 1
      },
      columnDefs: [
        {
          headerName: 'الاسم',
          field: 'label',
          checkboxSelection: true,
          floatingFilter: true
        },
        {
          headerName: 'الصنف',
          field: 'category.label',
          floatingFilter: true
        },
        {
          headerName: 'الملاحظات',
          field: 'note',
          floatingFilter: true
        }
      ]
    };
  }

  onGridReady($event) {
    this.gridApi = $event.api;
    this.columnApi = $event.columnApi;
  }


  addClient() {
    this.newClient = true;
  }

  private fetchData() {
    this.productService.getAllProducts().subscribe(res => {
      this.products = res;
      this.rowData = this.products;
      this.gridApi.setRowData(this.rowData);
    });
  }


  create(selectedRow?: IProduct) {
    const attribute = !!selectedRow ? selectedRow : DEFAULT_PRODUCT;

    const params: FormParams<IProduct> = {
      translate: this.translate,
      model: attribute,
      categories: this.categories
    } as FormParams<IProduct>;
    this.form = getProductFormDefinition(params);

    const modalRef = this.modalService.open(FormModalComponent);
    modalRef.componentInstance.form = this.form;
    modalRef.result.then(res => {
      if (res) {
        const procuct = cloneDeep(DEFAULT_PRODUCT);
        procuct.id = res.id;
        procuct.label = res.label;
        procuct.category = {id: res.category};
        this.productService.saveAllProducts([procuct]).subscribe(result => {
          this.fetchData();
        });
      }
    });
  }

  private deleteClient(id) {
    this.productService.deleteProduct(id).subscribe(res => {
      if (res) {
        this.fetchData();
      }
    });
  }

  getSelectedRows() {
    const selectedRow = this.gridApi.getSelectedRows();
  }

}
