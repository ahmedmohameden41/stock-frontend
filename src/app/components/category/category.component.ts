import {Component, OnInit} from '@angular/core';
import {ColumnApi, GridApi, GridOptions} from 'ag-grid-community';
import {DateFormControl, FileFormControl, MfForm, TextareaFormControl, TextboxFormControl, TextBoxType} from 'mf-dynamic-form';
import {CategoryService} from '../../services/category.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormModalComponent} from '../../shared/form-modal/form-modal.component';
import {FormParams, getCategoryFormDefinition} from '../../forms/category.-form';
import {TranslateService} from '@ngx-translate/core';
import {DEFAULT_CATEGORY, ICategory} from '../product/product.component';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  gridOptions: GridOptions;
  frameworkComponents: any;
  rowData: any[] = [];
  gridApi: GridApi;
  columnApi: ColumnApi;
  buttons: any[] = [];

  form: MfForm = {
    steps: [
      {
        sections: [
          {
            controls: [
              new TextboxFormControl({
                key: 'name',
                label: 'اسم الزبون',
                type: TextBoxType.TEXT
              }),
              new TextboxFormControl({
                key: 'countNumber',
                label: 'رقم الحساب',
                type: TextBoxType.TEXT,
              }),
              new DateFormControl({
                key: 'dateInscription',
                label: 'تاريخ فتح الحساب',
                type: Date
              }),
              new TextboxFormControl({
                key: 'nni',
                label: 'الرقم الوطني',
                type: TextBoxType.TEXT
              }),
              new FileFormControl({
                key: 'identityCard',
                label: 'بطاقة التعريف'
              }),
              new TextareaFormControl({
                key: 'note',
                label: 'الملاحطة',
                type: TextBoxType.TEXT
              })
            ]
          },
        ],
      },
    ]
  };
  categories: any[];

  constructor(private categoryService: CategoryService,
              private translate: TranslateService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.initAgGrid();
    this.fetchData();
  }


  initAgGrid() {
    this.buttons = [
      {
        title: 'زبون جديد',
        icon: 'fa fa-plus-circle',
        fn: () => this.create(),
        disabled: () => {
          return !this.gridApi;
        }
      },
      {
        title: 'تعديل معلومات الزبون',
        icon: 'fa fa-edit',
        fn: () => this.create(this.gridApi.getSelectedRows()[0]),
        disabled: () => this.gridApi && this.gridApi.getSelectedRows().length !== 1
      },
      {
        title: 'حذف الزبون',
        icon: 'fa fa-trash',
        fn: () => this.deleteCategpry(this.gridApi.getSelectedRows()[0].id),
        disabled: () => (this.gridApi && this.gridApi.getSelectedRows().length !== 1),
      }
    ];
    this.gridOptions = <GridOptions> {
      pagination: false,
      rowModelType: 'clientSide',
      enableColResize: true,
      enableSorting: true,
      enableFilter: true,
      defaultColDef: {
        resizable: true,
        flex: 1
      },
      columnDefs: [
        {
          headerName: 'الاسم',
          field: 'label',
          checkboxSelection: true,
          floatingFilter: true
        }
      ]
    };
  }

  onGridReady($event) {
    this.gridApi = $event.api;
    this.columnApi = $event.columnApi;
  }


  addClient() {
  }

  private fetchData() {
    this.categoryService.getAllCategories().subscribe(res => {
      this.categories = res;
      this.rowData = this.categories;
      this.gridApi.setRowData(this.rowData);
    });
  }

  create(selectedRow?: ICategory) {
    const attribute = !!selectedRow ? selectedRow : DEFAULT_CATEGORY;

    const params: FormParams<ICategory> = {
      translate: this.translate,
      model: attribute
    } as FormParams<ICategory>;
    this.form = getCategoryFormDefinition(params);

    const modalRef = this.modalService.open(FormModalComponent);
    modalRef.componentInstance.form = this.form;
    modalRef.result.then(res => {
      if (res) {
        this.categoryService.saveAllCategories([res]).subscribe(result => {
          this.fetchData();
        });
      }
    });
  }

  private deleteCategpry(id) {
    this.categoryService.deleteCategory(id).subscribe(res => {
      if (res) {
        this.fetchData();
      }
    });
  }

}
