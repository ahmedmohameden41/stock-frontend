import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {OrderService} from '../../../services/order.service';
import {StockService} from '../../../services/stock.service';
import {ProductService} from '../../../services/products.service';
import {IProduct} from '../../product/product.component';
import {SupplierService} from '../../../services/supplier.service';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss']
})
export class CreateOrderComponent implements OnInit {
  selectedCountries: any[];
  filteredCountries: any[];
  products: IProduct [] = [];
  orderItems: any[] = [];
  curentSupplier: any;
  suppliers: any[];
  order: any;



  constructor(
    public modal: NgbActiveModal,
    private productService: ProductService,
    public adherantService: OrderService,
    public supplierService: SupplierService
  ) {
  }

  ngOnInit() {
    this.getAllProducts();
    this.getAllSuppliers();
  }

  filterCountry(event) {
    let filtered: any[] = [];
    let query = event.query;
    for (let i = 0; i < this.products.length; i++) {
      let country = this.products[i];
      if (country.category.label.concat(country.label).toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country.category.label.concat('('+country.label+')'));
      }
    }

    this.filteredCountries = filtered;
  }

  getAllProducts() {
    this.productService.getAllProducts().subscribe(res => {
      this.products = res;
    });
  }

  getAllSuppliers() {
    this.supplierService.getAllSuppliers().subscribe(res => {
      this.suppliers = res;
    });
  }

  showItems() {
    this.order.orderItems = this.selectedCountries.map(
      product => {
        const pr = this.products.find(p => p.category.label.concat('('+p.label+')') === product);
        return pr;
      })
      .map(elem => {
        return {
          quantity: 1,
          purchasePrice: 0,
          salePrice: 0,
          price: 0,
          product: elem,
          status: '',
        };
      });
  }


  createOrder() {
    this.order.supplier = this.curentSupplier;
    this.modal.close(this.order);
  }

  updatePrice(orderItem: any) {
    orderItem.price = orderItem.quantity * orderItem.purchasePrice;
    this.order.totalAmount = 0;
    this.order.orderItems.forEach(elem => {
      this.order.totalAmount = this.order.totalAmount + elem.price;
    })
  }
}
