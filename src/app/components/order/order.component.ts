import {Component, OnInit, ViewChild} from '@angular/core';
import {ColumnApi, GridApi, GridOptions} from 'ag-grid-community';
import {IProduct} from '../product/product.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from '../../services/products.service';
import {StockService} from '../../services/stock.service';
import {OrderService} from '../../services/order.service';
import {AgGridActionsButtonsComponent} from '../../shared/ag-grid-actions-buttons/ag-grid-actions-buttons.component';
import {SupplierService} from '../../services/supplier.service';
import {OrderDetailComponent} from './order-detail/order-detail.component';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DetailsInvoiceComponent} from '../adherant/details-invoice/details-invoice.component';
import {CreateOrderComponent} from './create-order/create-order.component';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  // Create DaiDH
  @ViewChild('multiSelect') multiSelect;
  public form: FormGroup;
  public loadContent: boolean = false;
  public name = 'Cricketers';
  public data = [];
  public settings = {};
  public selectedItems = [];


  gridOptions: GridOptions;
  frameworkComponents: any;
  rowData;
  gridApi: GridApi;
  columnApi: ColumnApi;
  products: IProduct [] = [];
  factureType: string = 'FactOui';
  suppliers: any[];
  orderItems: any[] = [];
  stocks: any[];
  orderItem: any = {
    quantity: 0,
    purchasePrice: 0,
    salePrice: 0,
    price: 0,
    product: null,
    status: '',
  };
  totalAmount: number = 0;
  remarque: string = '';
  curentSupplier: any;
  order: { partialAmount: number; totalAmount: number; note: string; supplier: any; orderItems: any[]; status: string } =
    {partialAmount: 0, totalAmount: 0, note: '', supplier: null, orderItems: [], status: ''};
  rowSelection;

  filteredCountries: any[];

  selectedCountries: any[];

  selectedCountryAdvanced: any[];

  filteredBrands: any[];


  filteredGroups: any[];

  constructor(private modalService: NgbModal,
              private productService: ProductService,
              private supplierService: SupplierService,
              private stockService: StockService,
              private orderService: OrderService) {
    this.frameworkComponents = {
      buttonRenderer: AgGridActionsButtonsComponent,
    };
  }


  ngOnInit() {
    this.initAgGrid();
    this.fetchOrders();
    this.getAllProducts();
    this.getAllClients();

    this.setForm();
  }

  initAgGrid() {

    this.gridOptions = <GridOptions> {
      pagination: false,
      rowModelType: 'clientSide',
      enableColResize: true,
      enableSorting: true,
      enableFilter: true,
      defaultColDef: {
        resizable: true
      },
      columnDefs: [{
        headerName: 'المورد',
        field: 'supplier.name',
        floatingFilter: true,
        checkboxSelection: true
      },
        {
          headerName: 'تاريخ الطلبية',
          field: 'dateCreation',
          floatingFilter: true
        },
        {
          headerName: 'الملاحظة',
          field: 'note',
          floatingFilter: true
        },
        {
          headerName: 'المبلغ',
          field: 'totalAmount',
          floatingFilter: true
        },
        {
          headerName: 'الحالة',
          field: 'status',
          floatingFilter: true
        }
      ]
    };
    this.buttons = [
      {
        title: 'detail',
        icon: 'fa fa-edit',
        fn: () => this.detail(this.gridApi.getSelectedRows()[0]),
        disabled: () => this.gridApi && this.gridApi.getSelectedRows().length !== 1
      },
      {
        title: 'حذف',
        icon: 'fa fa-trash',
        fn: () => this.deleteOrder(this.gridApi.getSelectedRows()[0].id),
        disabled: () => (this.gridApi && this.gridApi.getSelectedRows().length !== 1),
      }
    ];
    this.rowSelection = 'single';
  }

  onGridReady($event) {
    this.gridApi = $event.api;
    this.columnApi = $event.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  private fetchOrders() {
    this.orderService.getAllOrders().subscribe(orders => {
      this.rowData = orders;
    });
  }

  newFacture() {
    this.order = {
      partialAmount: this.order.partialAmount,
      supplier: this.curentSupplier,
      status: 'COMMANDE',
      totalAmount: this.totalAmount, note: this.remarque, orderItems: this.orderItems
    };


    const modalRef = this.modalService.open(CreateOrderComponent, {size: 'lg'});
    modalRef.componentInstance.order = this.order;

    modalRef.result.then(res => {
      this.orderService.saveAdherant(res).subscribe(grp => {
        this.fetchOrders();
      });
    });
  }

  getAllProducts() {
    this.productService.getAllProducts().subscribe(res => {
      this.products = res;
    });
  }


  getAllClients() {
    this.supplierService.getAllSuppliers().subscribe(res => {
      this.suppliers = res;
    });
  }


  addElement() {
    const productNames = this.orderItems.map(element => element.product.label);
    if (!productNames.includes(this.orderItem.serie)) {
      this.orderItems.push(this.orderItem);
      this.totalAmount = this.totalAmount + (this.orderItem.purchasePrice * this.orderItem.quantity);
      this.orderItem = {
        quantity: 0,
        purchasePrice: 0,
        salePrice: 0,
        price: 0,
        product: null,
        status: '',
      };
    }
  }

  detail(selectedRow) {
    const modalRef = this.modalService.open(OrderDetailComponent, {size: 'lg'});
    modalRef.componentInstance.selectedRow = selectedRow;
  }


  public setForm() {
    this.form = new FormGroup({
      name: new FormControl(this.data, Validators.required)
    });
    this.loadContent = true;
  }

  text: string;

  results: string[];
  buttons: any[] = [];

  get f() {
    return this.form.controls;
  }

  public save() {
    console.log(this.form.value);
  }

  public onDeSelect(item: any) {
    console.log(item);
  }

  public onSelectAll(items: any) {
    console.log(items);
  }

  private deleteOrder(id) {
    this.orderService.deleteOrder(id).subscribe(res => {
      if (res) {
        this.fetchOrders();
      }
    });
  }
}
