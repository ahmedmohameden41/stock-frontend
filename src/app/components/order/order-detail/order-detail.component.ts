import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Adherant, MODALS} from '../../../shared/upload-modal/upload-modal.component';
import {SupplierService} from '../../../services/supplier.service';
import {OrderService} from '../../../services/order.service';
import {StockService} from '../../../services/stock.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {
  selectedRow: any;
  orderItems: any[];

  constructor(
    public modal: NgbActiveModal,
    private orderService: OrderService,
    public adherantService: OrderService,
    public stockService: StockService
  ) {
  }

  ngOnInit() {
    if (this.selectedRow) {
      this.orderItems = this.selectedRow.orderItems.map(elem => {
        elem.salePrice = 0;
        return elem;
      });
    }
  }


  validOrder() {
    const orderItemsNumber = this.orderItems.filter(item => item.salePrice === 0);
    if (orderItemsNumber.length !== 0) {
      return;
    }
    this.selectedRow.status = 'STOCK';
    this.orderService.saveAdherant(this.selectedRow).subscribe(grp => {
      this.modal.close(MODALS.DISMISS);
    });
  }
}
