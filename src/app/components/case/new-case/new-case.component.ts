import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {OrderService} from '../../../services/order.service';

@Component({
    selector: 'app-new-case',
    templateUrl: './new-case.component.html',
    styleUrls: ['./new-case.component.scss']
})
export class NewCaseComponent implements OnInit {
    marked: boolean;

    @Input()
    transaction: any;

    constructor(
        public modal: NgbActiveModal,
        public adherantService: OrderService,
    ) {
    }

    ngOnInit() {
    }


    createOrder() {
        this.modal.close(this.transaction);
    }

    toggleVisibility(e) {
        this.marked = e.target.checked;
    }

    selectCheckBox(b: boolean) {
        this.transaction.toMe = b;
    }
}
