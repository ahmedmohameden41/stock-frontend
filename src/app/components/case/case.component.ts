import {Component, OnInit} from '@angular/core';
import {ColumnApi, GridApi, GridOptions} from 'ag-grid-community';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AgGridActionsButtonsComponent} from '../../shared/ag-grid-actions-buttons/ag-grid-actions-buttons.component';
import {CreateTransactionComponent} from '../transaction/create-transaction/create-transaction.component';
import {DetailsInvoiceComponent} from '../adherant/details-invoice/details-invoice.component';
import {CaseService} from '../../services/case.service';
import {NewCaseComponent} from './new-case/new-case.component';

@Component({
  selector: 'app-case',
  templateUrl: './case.component.html',
  styleUrls: ['./case.component.scss']
})
export class CaseComponent implements OnInit {
  gridOptions: GridOptions;
  frameworkComponents: any;
  rowData;
  gridApi: GridApi;
  columnApi: ColumnApi;
  rowSelection;
  buttons: any[];
  transaction: {
    id: number,
    date: any,
    note: string,
    toMe: boolean,
    amount: number,
    devise: number
  } = {
    id: null,
    date: null,
    note: '',
    toMe: false,
    amount: 0,
    devise: 0
  };

  constructor(private modalService: NgbModal,
              private caseService: CaseService) {
    this.frameworkComponents = {
      buttonRenderer: AgGridActionsButtonsComponent,
    };
  }

  ngOnInit() {
    this.initAgGrid();
    this.loadCases();
  }

  initAgGrid() {
    this.gridOptions = <GridOptions> {
      pagination: false,
      rowModelType: 'clientSide',
      enableColResize: true,
      enableSorting: true,
      enableFilter: true,
      defaultColDef: {
        resizable: true
      },
      columnDefs: [
        {
          headerName: 'التاريخ',
          field: 'dateCreation',
          floatingFilter: true,
          checkboxSelection: true
        },
        {
          headerName: 'الملاحظة',
          field: 'note',
          floatingFilter: true
        },
        {
          headerName: 'المعاملة',
          field: 'toMe',
          cellRenderer: params => params?.value ? 'دخل' : 'صرف'

        },
        {
          headerName: 'المبلغ بالأوقية',
          field: 'amount',
          floatingFilter: true
        },
        {
          headerName: 'المبلغ بالدرهم',
          field: 'devise',
          floatingFilter: true
        }
      ]
    };
    this.buttons = [
      {
        title: 'detail',
        icon: 'fa fa-edit',
        fn: () => this.detail(this.gridApi.getSelectedRows()[0]),
        disabled: () => this.gridApi && this.gridApi.getSelectedRows().length !== 1
      },
      {
        title: 'حذف',
        icon: 'fa fa-trash',
        fn: () => this.deleteTransaction(this.gridApi.getSelectedRows()[0].id),
        disabled: () => (this.gridApi && this.gridApi.getSelectedRows().length !== 1),
      }
    ];
    this.rowSelection = 'single';
  }

  onGridReady($event) {
    this.gridApi = $event.api;
    this.columnApi = $event.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  private loadCases() {
    this.caseService.getAllCases().subscribe(transactions => {
      this.rowData = transactions.map(tr => {
        return {
          id: tr.id,
          note: tr.note,
          toMe: tr.toMe,
          amount: tr.amount,
          devise: tr.devise,
          dateCreation: tr.dateCreation
        };
      });
    });
  }

  newCases() {
    const modalRef = this.modalService.open(NewCaseComponent, {size: 'lg'});
    modalRef.componentInstance.transaction = this.transaction;

    modalRef.result.then(res => {
      this.caseService.saveCase(res).subscribe(res => {
        this.loadCases();
      });
    }).catch(error => console.log(error));
  }


  detail(invoice) {
    this.transaction = invoice;
    this.newCases();
  }

  private deleteTransaction(id) {
    this.caseService.deleteCase(id).subscribe(res => {
      if (res) {
        this.loadCases();
      }
    });
  }
}
