import {Component, OnInit} from '@angular/core';
import {Client} from '../client/client.component';
import {cloneDeep} from 'lodash';
import {SupplierService} from '../../services/supplier.service';
import {DetailsClientComponent} from '../client/details-client/details-client.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrderService} from '../../services/order.service';

export class Supplier {
  name: string;
  nameOfCompany: string;
  phoneNumber: string;
  address: string;
  note: string;
  dateInscription: any;
}


@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.scss']
})
export class SupplierComponent implements OnInit {
  suppliers: any[] = [];
  supplierForm: boolean;
  isNewSupplier: boolean;
  newSupplier: Supplier = new Supplier();
  editSupplierForm: boolean;
  editSupplier: Supplier = new Supplier();

  constructor(private supplierService: SupplierService,
              private orderService: OrderService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.geAllSuppliers();
  }

  geAllSuppliers() {
    this.supplierService.getAllSuppliers().subscribe(res => {
      this.suppliers = res;
    });
  }


  showEditGroupeForm(client: Client) {
    if (!client) {
      this.supplierForm = false;
      return;
    }
    this.editSupplierForm = true;
    this.editSupplier = cloneDeep(client);
  }

  showAddClientForm() {
    // resets form if edited user
    if (this.suppliers.length) {
      this.newSupplier = new Supplier();
    }
    this.supplierForm = true;
    this.isNewSupplier = true;
  }

  saveClient(supplier: Supplier) {
    if (this.isNewSupplier) {
      this.supplierService.saveAllSuppliers([supplier]).subscribe(grp => {
        this.geAllSuppliers();
      });
    }
    this.supplierForm = false;
  }

  updateClient() {
    this.supplierService.saveAllSuppliers([this.editSupplier]).subscribe(res => {
      this.geAllSuppliers();
    });
    this.editSupplierForm = false;
    this.editSupplier = new Supplier();
  }

  removeGroupe(supplier: Supplier) {

  }

  cancelEdits() {
    this.editSupplier = new Supplier();
    this.editSupplierForm = false;
  }

  cancelNewGroupe() {
    this.newSupplier = new Client();
    this.supplierForm = false;
  }

  detailSupplier(supplier) {
    this.orderService.getAllOrderBySupplierId(supplier.id).subscribe(res => {
      const modalRef = this.modalService.open(DetailsClientComponent, {size: 'lg'});
      modalRef.componentInstance.selectedRow = supplier;
      modalRef.componentInstance.businesType = 'SUPPLIER';
      modalRef.componentInstance.overridedTransactions = res?.map(elem => {
        return {
          note: elem.note,
          toMe: false,
          businesType: 'CLIENT',
          businesId: elem.id,
          amount: 0,
          devise: elem.totalAmount,
          dateCreation: elem.dateCreation
        };
      });
    });
  }
}
