import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ProductService} from '../../services/products.service';
import {IProduct} from '../product/product.component';
import {StockService} from '../../services/stock.service';

export interface IStock {
  id: number;
  product: IProduct;
  quantity: number;
  quantitySold: number;
  returnedQuantity: number;
  salePrice: number;
};

export const initailStock: IStock = {
  quantity: 0, product: null, id: null, salePrice: 0, quantitySold: 0, returnedQuantity: 0
};


@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {
  pageTitle: string = 'المخزن';
  imageWidth: number = 50;
  imageMargin: number = 2;
  showImage: boolean = false;
  stock: IStock = initailStock;

  stockForm = this.fb.group({
    id: [null],
    quantity: [0, Validators.required],
    product: [null, Validators.required]
  });


  onSubmit() {

  }


  filteredStocks: IStock[];
  products: IProduct [] = [];

  // I want to set default values
  stocks: any[] = [];
  categoryLabel: string = '';

  constructor(private stockService: StockService,
              private productService: ProductService,
              private fb: FormBuilder) {
  }


  onRatingClicked(message: string): void {
    this.pageTitle = 'Stock List: ' + message;
  }

  ngOnInit(): void {
    this.getAllStocks();
    this.getAllProducts();
  }

  getAllStocks() {
    this.stockService.getAllStocks().subscribe(res => {
      this.stocks = res;
      this.filteredStocks = this.stocks;
    });
  }

  editStock() {
  }

  private getAllProducts() {
    this.productService.getAllProducts().subscribe(res => {
      this.products = res;
    });
  }
}
