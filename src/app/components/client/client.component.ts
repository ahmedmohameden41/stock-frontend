import {Component, OnInit} from '@angular/core';
import {GroupeService} from '../../services/groupe.service';
import {cloneDeep} from 'lodash';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DetailsClientComponent} from './details-client/details-client.component';
import {InvoiceService} from '../../services/invoice.service';

export class Client {
  name: string;
  nameOfCompany: string;
  phoneNumber: string;
  address: string;
  note: string;
  dateInscription: any;
}

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  clients: any[] = [];
  clientForm: boolean;
  isNewClient: boolean;
  newClient: Client = new Client();
  editClientForm: boolean;
  editClient: Client = new Client();

  constructor(private groupService: GroupeService, private invoiceService: InvoiceService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.geAllClients();
  }

  geAllClients() {
    this.groupService.getAllClients().subscribe(res => {
      this.clients = res;
    });
  }


  showEditGroupeForm(client: Client) {
    if (!client) {
      this.clientForm = false;
      return;
    }
    this.editClientForm = true;
    this.editClient = cloneDeep(client);
  }

  showAddClientForm() {
    // resets form if edited user
    if (this.clients.length) {
      this.newClient = new Client();
    }
    this.clientForm = true;
    this.isNewClient = true;
  }

  saveClient(client: Client) {
    if (this.isNewClient) {
      this.groupService.saveAllGroupe([client]).subscribe(grp => {
        this.geAllClients();
      });
    }
    this.clientForm = false;
  }

  updateClient() {
    this.groupService.saveAllGroupe([this.editClient]).subscribe(res => {
      this.geAllClients();
    });
    this.editClientForm = false;
    this.editClient = new Client();
  }

  removeGroupe(user: Client) {

  }

  cancelEdits() {
    this.editClient = new Client();
    this.editClientForm = false;
  }

  cancelNewGroupe() {
    this.newClient = new Client();
    this.clientForm = false;
  }


  detailClient(client) {
    this.invoiceService.getAllByClientId(client.id).subscribe(res => {
      const modalRef = this.modalService.open(DetailsClientComponent, {size: 'lg'});
      modalRef.componentInstance.selectedRow = client;
      modalRef.componentInstance.businesType = 'CLIENT';
      modalRef.componentInstance.overridedTransactions = res.map(elem => {
        return {
          note: elem.note,
          toMe: true,
          businesType: 'CLIENT',
          businesId: elem.id,
          amount: 0,
          devise: elem.totalAmount,
          dateCreation: elem.dateCreation
        };
      });
    });
  }
}
