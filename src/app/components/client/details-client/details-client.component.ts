import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TransactionService} from '../../../services/transaction.service';
import {cloneDeep} from 'lodash';


@Component({
  selector: 'app-details-client',
  templateUrl: './details-client.component.html',
  styleUrls: ['./details-client.component.scss']
})
export class DetailsClientComponent implements OnInit {

  selectedRow: any;
  businesType: any;
  transactionItems: any[];
  transactionItemsFiltered: any[] = [];
  overridedTransactions: any[] = [];

  @ViewChild('pdfTable') pdfTable: ElementRef;
  downloadPdf: string = 'طباعة الفاتورة';
  totalAmount: number = 0;
  totalDevise: number = 0;
  totalAmountBefor: number = 0;
  totalDeviseBefor: number = 0;
  dateDebut: any;
  dateFin: any;

  constructor(
    public modal: NgbActiveModal,
    public transactionService: TransactionService) {
  }

  ngOnInit() {
    if (this.selectedRow) {
      this.transactionService.getAllTransactionByBusinesTypeAndBusinesId
      (this.businesType, this.selectedRow.id).subscribe(res => {
        this.transactionItems = res;
        this.transactionItems.forEach(t => {
          if(t.toMe){
            this.totalAmount = this.totalAmount + t.amount;
            this.totalDevise = this.totalDevise + t.devise;
          }else {
            this.totalAmount = this.totalAmount - t.amount;
            this.totalDevise = this.totalDevise - t.devise;
          }
        });
        this.overridedTransactions.forEach(tr => {
          if(tr.toMe){
            this.totalDevise = this.totalDevise + tr.devise;
          }else {
            this.totalDevise = this.totalDevise - tr.devise;
          }
          this.transactionItems.push(tr);
        });
        this.transactionItemsFiltered = cloneDeep(this.transactionItems);
      });
    }
  }

  updateDate() {
    if (!this.dateFin || !this.dateDebut) {
      return;
    }
    this.totalAmountBefor = 0;
    this.totalDeviseBefor = 0;

    cloneDeep(this.transactionItems).filter(e => e.dateCreation < this.dateDebut).forEach(t => {
      if(t.toMe){
        this.totalAmountBefor = this.totalAmountBefor + t.amount;
        this.totalDeviseBefor = this.totalDeviseBefor + t.devise;
      }else {
        this.totalAmountBefor = this.totalAmountBefor - t.amount;
        this.totalDeviseBefor = this.totalDeviseBefor - t.devise;
      }
    });
    this.totalAmount = 0;
    this.totalDevise = 0;

    this.transactionItemsFiltered = cloneDeep(this.transactionItems)
      .filter(e => e.dateCreation >= this.dateDebut && e.dateCreation <= this.dateFin);

    cloneDeep(this.transactionItems)
      .filter(e => e.dateCreation <= this.dateFin)
      .forEach(t => {
      if(t.toMe){
        this.totalAmount = this.totalAmount + t.amount;
        this.totalDevise = this.totalDevise + t.devise;
      }else {
        this.totalAmount = this.totalAmount - t.amount;
        this.totalDevise = this.totalDevise - t.devise;
      }
    })
  }
}
