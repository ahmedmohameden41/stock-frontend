import {Component, OnInit} from '@angular/core';
import {ColumnApi, GridApi, GridOptions} from 'ag-grid-community';
import {IProduct} from '../product/product.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from '../../services/products.service';
import {SupplierService} from '../../services/supplier.service';
import {StockService} from '../../services/stock.service';
import {OrderService} from '../../services/order.service';
import {AgGridActionsButtonsComponent} from '../../shared/ag-grid-actions-buttons/ag-grid-actions-buttons.component';
import {OrderDetailComponent} from '../order/order-detail/order-detail.component';
import {AvoirService} from '../../services/avoir.service';

@Component({
  selector: 'app-avoir',
  templateUrl: './avoir.component.html',
  styleUrls: ['./avoir.component.scss']
})
export class AvoirComponent implements OnInit {

  gridOptions: GridOptions;
  frameworkComponents: any;
  rowData;
  gridApi: GridApi;
  columnApi: ColumnApi;
  products: IProduct [] = [];
  factureType: string = 'FactOui';
  suppliers: any[];
  avoirItems: any[] = [];
  stocks: any[];
  totalAmount: number = 0;
  remarque: string = '';
  curentSupplier: any;
  avoir: { totalAmount: number; note: string; supplier: any; avoirItems: any[] };
  rowSelection;
  avoirItem: any = {
    quantity: 0,
    price: 0,
    product: null,
  };

  constructor(private modalService: NgbModal,
              private productService: ProductService,
              private supplierService: SupplierService,
              private stockService: StockService,
              private avoirService: AvoirService) {
    this.frameworkComponents = {
      buttonRenderer: AgGridActionsButtonsComponent,
    };
  }

  ngOnInit() {
    this.initAgGrid();
    this.loadAvoirs();
    this.getAllProducts();
    this.getAllClients();
    this.getAllStocks();
  }

  initAgGrid() {
    this.gridOptions = <GridOptions> {
      pagination: false,
      rowModelType: 'clientSide',
      enableColResize: true,
      enableSorting: true,
      enableFilter: true,
      defaultColDef: {
        resizable: true
      },
      columnDefs: [{
        headerName: 'Supplier Name',
        field: 'supplier.name',
        floatingFilter: true
      },
        {
          headerName: 'Date Creation',
          field: 'dateCreation',
          floatingFilter: true
        },
        {
          headerName: 'Note',
          field: 'note',
          floatingFilter: true
        },
        {
          headerName: 'Total Amount',
          field: 'totalAmount',
          floatingFilter: true
        },
        {
          headerName: 'Staut',
          field: 'status',
          floatingFilter: true
        }
      ]
    };
    this.rowSelection = 'single';
  }

  onGridReady($event) {
    this.gridApi = $event.api;
    this.columnApi = $event.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  private loadAvoirs() {
    this.avoirService.getAllAvoirs().subscribe(adherants => {
      this.rowData = adherants;
    });
  }

  newFacture() {
    this.avoir = {
      supplier: this.curentSupplier,
      totalAmount: this.totalAmount, note: this.remarque, avoirItems: this.avoirItems
    };

    if (this.curentSupplier != null && this.totalAmount !== 0) {
      this.avoirService.saveAvoir(this.avoir).subscribe(grp => {
        this.remarque = '';
        this.totalAmount = 0;
        this.curentSupplier = null;
        this.avoirItem = {
          quantity: 0,
          price: 0,
          product: null,
        };
        this.avoirItems = [];
        this.loadAvoirs();
      });
    }
  }

  getAllProducts() {
    this.productService.getAllProducts().subscribe(res => {
      this.products = res;
    });
  }


  getAllClients() {
    this.supplierService.getAllSuppliers().subscribe(res => {
      this.suppliers = res;
    });
  }


  addElement() {
    const productSeries = this.avoirItems.map(element => element.product.label);
    if (!productSeries.includes(this.avoirItem.product.label)) {
      this.avoirItems.push(this.avoirItem);
      this.totalAmount = this.totalAmount + (this.avoirItem.price * this.avoirItem.quantity);

      this.avoirItem = {
        quantity: 1,
        price: 0,
        product: null,
      };
    }
  }

  private getAllStocks() {
    this.stockService.getAllStocks().subscribe(res => {
      this.stocks = res;
    });
  }

  appendCurentElement() {
    const itemSelected = this.stocks.filter(elem => elem.product.label === this.avoirItem.product.label)[0];
    this.avoirItem.price = itemSelected.purchasePrice;
    // this.maxQuantity = itemSelected.quantity - itemSelected.quantitySold;
    this.avoirItem.orderItem = {id: itemSelected.id};
  }

  detail($event) {
    const modalRef = this.modalService.open(OrderDetailComponent, {size: 'lg'});
    modalRef.componentInstance.selectedRow = this.gridApi.getSelectedRows()[0];

  }

}
