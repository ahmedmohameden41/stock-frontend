import {Component, OnInit} from '@angular/core';
import {ColumnApi, GridApi, GridOptions} from 'ag-grid-community';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AgGridActionsButtonsComponent} from '../../shared/ag-grid-actions-buttons/ag-grid-actions-buttons.component';
import {ProductService} from '../../services/products.service';
import {IProduct} from '../product/product.component';
import {GroupeService} from '../../services/groupe.service';
import {StockService} from '../../services/stock.service';
import {InvoiceService} from '../../services/invoice.service';
import {cloneDeep} from 'lodash';
import {OrderDetailComponent} from '../order/order-detail/order-detail.component';
import {DetailsInvoiceComponent} from './details-invoice/details-invoice.component';
import {CreateOrderComponent} from '../order/create-order/create-order.component';
import {CreateVoiceComponent} from './create-voice/create-voice.component';

@Component({
  selector: 'app-adherant',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  gridOptions: GridOptions;
  frameworkComponents: any;
  rowData;
  gridApi: GridApi;
  columnApi: ColumnApi;
  products: IProduct [] = [];
  factureType: string = 'FactOui';
  clients: any[];
  orders: any[] = [];
  invoiceItems: any[] = [];
  invoiceItem: any = {
    orderItem: null,
    quantity: 0,
    purchasePrice: 0,
    salePrice: 0,
    price: 0,
    product: null,
    status: '',
  };
  totalAmount: number = 0;
  remarque: string = '';
  curentClient: any;
  invoice: { status: boolean; partialAmount: number; totalAmount: number; note: string; invoiceItems: any[]; client: any } =
    {status: null, partialAmount: 0, totalAmount: 0, note: '', invoiceItems: [], client: null};
  stocks: any[] = [];
  rowSelection;
  maxQuantity: number = 0;
  status: boolean;
  buttons: any[];

  constructor(private modalService: NgbModal,
              private productService: ProductService,
              private clientService: GroupeService,
              private stockService: StockService,
              private invoiceService: InvoiceService) {
    this.frameworkComponents = {
      buttonRenderer: AgGridActionsButtonsComponent,
    };
  }

  ngOnInit() {
    this.invoice.partialAmount = 0;
    this.initAgGrid();
    this.loadInvoices();
    this.getAllProducts();
    this.getAllClients();
    this.getAllStocks();
  }

  initAgGrid() {
    this.gridOptions = <GridOptions> {
      pagination: false,
      rowModelType: 'clientSide',
      enableColResize: true,
      enableSorting: true,
      enableFilter: true,
      defaultColDef: {
        resizable: true
      },
      columnDefs: [{
        headerName: 'الزبون',
        field: 'client.name',
        floatingFilter: true,
        checkboxSelection: true
      },
        {
          headerName: 'التاريخ',
          field: 'dateCreation',
          floatingFilter: true
        },
        {
          headerName: 'الملاحظة',
          field: 'note',
          floatingFilter: true
        },
        {
          headerName: 'الحالة',
          field: 'status',
          floatingFilter: true
        },
        {
          headerName: 'المبلغ',
          field: 'totalAmount',
          floatingFilter: true
        }
      ]
    };
    this.buttons = [
      {
        title: 'detail',
        icon: 'fa fa-edit',
        fn: () => this.detail(this.gridApi.getSelectedRows()[0]),
        disabled: () => this.gridApi && this.gridApi.getSelectedRows().length !== 1
      },
      {
        title: 'حذف',
        icon: 'fa fa-trash',
        fn: () => this.deleteInvoice(this.gridApi.getSelectedRows()[0].id),
        disabled: () => (this.gridApi && this.gridApi.getSelectedRows().length !== 1),
      }
    ];
    this.rowSelection = 'single';
  }

  onGridReady($event) {
    this.gridApi = $event.api;
    this.columnApi = $event.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  private loadInvoices() {
    this.invoiceService.getAllInvoices().subscribe(invoices => {
      this.rowData = invoices;
    });
  }

  newFacture() {
    this.invoice = {
      status: null,
      partialAmount: this.invoice.partialAmount,
      invoiceItems: [],
      client: this.curentClient,
      totalAmount: this.totalAmount, note: this.remarque
    };

    const modalRef = this.modalService.open(CreateVoiceComponent, {size: 'lg'});
    modalRef.componentInstance.invoice = this.invoice;
    modalRef.componentInstance.stocks = this.stocks;

    modalRef.result.then(res => {
      this.invoiceService.saveInvoices(res).subscribe(grp => {
        this.remarque = '';
        this.totalAmount = 0;
        this.curentClient = null;
        this.invoiceItems = [];
        this.loadInvoices();
      });
    });
  }

  getAllProducts() {
    this.productService.getAllProducts().subscribe(res => {
      this.products = res;
    });
  }


  getAllClients() {
    this.clientService.getAllClients().subscribe(res => {
      this.clients = res;
    });
  }


  private getAllStocks() {
    this.stockService.getAllStocks().subscribe(res => {
      this.stocks = res;
    });
  }

  detail(invoice) {
    const modalRef = this.modalService.open(DetailsInvoiceComponent, {size: 'lg'});
    modalRef.componentInstance.selectedRow = invoice;
  }

  private deleteInvoice(id) {
    this.invoiceService.deleteInvoice(id).subscribe(res => {
      if (res) {
        this.loadInvoices();
      }
    });
  }
}
