import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {OrderService} from '../../../services/order.service';
import {StockService} from '../../../services/stock.service';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import htmlToPdfmake from 'html-to-pdfmake';
import {getLocaleDirection} from '@angular/common';

pdfMake.vfs = pdfFonts.pdfMake.vfs;
pdfMake.fonts = {
  'Cairo': {
    normal: 'http://fonts.gstatic.com/s/cairo/v1/-tPnHq7mmAjcjJRSjsuZGA.ttf',
    bold: 'http://fonts.gstatic.com/s/cairo/v1/ONxTSBYfmg-V5CkIwS_5gQ.ttf',
  }
};

@Component({
  selector: 'app-details-invoice',
  templateUrl: './details-invoice.component.html',
  styleUrls: ['./details-invoice.component.scss']
})
export class DetailsInvoiceComponent implements OnInit {
  selectedRow: any;
  invoiceItems: any[];

  @ViewChild('pdfTable') pdfTable: ElementRef;
  downloadPdf: string = 'طباعة الفاتورة';

  constructor(
    public modal: NgbActiveModal,
    public adherantService: OrderService,
  ) {
  }

  ngOnInit() {
    if (this.selectedRow) {
      this.invoiceItems = this.selectedRow.invoiceItems;
    }
  }

  public downloadAsPDF() {
    //get table html
    const pdfTable = this.pdfTable.nativeElement;
    //html to pdf format
    var html = htmlToPdfmake(pdfTable.innerHTML.fixed());


    const documentDefinition = {
      content: html,
      direction: 'enableRtl',
      defaultStyle: {
        font: 'Cairo',
        direction: 'rtl'
      }
    };
    this.downloadPdf = '';
    pdfMake.createPdf(documentDefinition).open();
    this.downloadPdf = 'طباعت الفاتورة';
  }
}
