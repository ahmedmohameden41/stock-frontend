import {Component, Input, OnInit} from '@angular/core';
import {IProduct} from '../../product/product.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from '../../../services/products.service';
import {OrderService} from '../../../services/order.service';
import {SupplierService} from '../../../services/supplier.service';
import {GroupeService} from '../../../services/groupe.service';

@Component({
  selector: 'app-create-voice',
  templateUrl: './create-voice.component.html',
  styleUrls: ['./create-voice.component.scss']
})
export class CreateVoiceComponent implements OnInit {

  selectedCountries: any[];
  filteredCountries: any[];
  products: IProduct [] = [];
  invoiceItems: any[] = [];
  curentClient: any;
  clients: any[];
  invoice: any;

  @Input()
  stocks: any[];
  items: any[];
  item: any;
  selectedCities3: any[];
  cities: any[];
  maxQuantity: boolean = false;
  note: string = '';


  constructor(
    public modal: NgbActiveModal,
    private productService: ProductService,
    public adherantService: OrderService,
    public clientService: GroupeService
  ) {
    this.cities = [
      {name: 'New York', code: 'NY'},
      {name: 'Rome', code: 'RM'},
      {name: 'London', code: 'LDN'},
      {name: 'Istanbul', code: 'IST'},
      {name: 'Paris', code: 'PRS'}
    ];
  }

  ngOnInit() {
    this.products = this.stocks.map(elem => elem.product);
    this.getAllSuppliers();
    this.items = [];
    for (let i = 0; i < 10000; i++) {
      this.items.push({label: 'Item ' + i, value: 'Item ' + i});
    }
  }

  filterCountry(event) {
    let filtered: any[] = [];
    let query = event.query;
    for (let i = 0; i < this.products.length; i++) {
      let country = this.products[i];
      if (country.category.label.concat(country.label).toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country.category.label.concat('(' + country.label + ')'));
      }
    }

    this.filteredCountries = filtered;
  }

  getAllSuppliers() {
    this.clientService.getAllClients().subscribe(res => {
      this.clients = res;
    });
  }

  showItems() {
    this.invoice.invoiceItems = this.selectedCountries.map(
      product => {
        const pr = this.products.find(p => p.category.label.concat('(' + p.label + ')') === product);
        return pr;
      })
      .map(elem => {
        const orderItem = this.stocks.find(item => item.product.id === elem.id);
        this.invoice.totalAmount = this.invoice.totalAmount + orderItem.salePrice;
        return {
          quantity: 1,
          purchasePrice: 0,
          salePrice: orderItem.salePrice,
          price: 0,
          product: elem,
          orderItem,
          status: '',
        };
      });

    this.updateTotalAmount();
  }


  createOrder() {
    this.invoice.client = this.curentClient[0];
    this.invoice.note = this.note;
    this.modal.close(this.invoice);
  }

  updatePrice(orderItem: any) {
    const itmOrder = this.stocks.find(item => item.product.id === orderItem.product.id);
    if (orderItem.quantity >
      (itmOrder.quantity - (itmOrder.quantitySold + itmOrder.returnedQuantity))) {
      this.maxQuantity = true;
    } else {
      this.maxQuantity = false;
    }
    this.updateTotalAmount();
  }

  updateTotalAmount(){
    this.invoice.totalAmount = 0;
    this.invoice.invoiceItems.forEach(elem => {
      this.invoice.totalAmount = this.invoice.totalAmount +  (elem.quantity * elem.salePrice);
    });
  }

}
