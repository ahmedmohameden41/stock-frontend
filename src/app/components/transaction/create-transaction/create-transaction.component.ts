import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from '../../../services/products.service';
import {OrderService} from '../../../services/order.service';
import {GroupeService} from '../../../services/groupe.service';
import {SupplierService} from '../../../services/supplier.service';

@Component({
  selector: 'app-create-transaction',
  templateUrl: './create-transaction.component.html',
  styleUrls: ['./create-transaction.component.scss']
})
export class CreateTransactionComponent implements OnInit {


  clients: any[];
  suppliers: any[];
  invoice: any;
  selectedRow: {
    id: number;
    note: string;
    owner: string;
    toMe: boolean;
    devise: number;
    amount: number;
    curentClient: any;
    curentSupplier: any;
  } = {
    id: null,
    note: '',
    owner: 'CLIENT',
    toMe: false,
    devise: 0,
    amount: 0,
    curentClient: [],
    curentSupplier: [],
  };
  marked: boolean;


  items: any[];
  item: any;
  selectedCities3: any[];
  cities: any[];


  constructor(
    public modal: NgbActiveModal,
    private productService: ProductService,
    private supplierService: SupplierService,
    public adherantService: OrderService,
    public clientService: GroupeService
  ) {
  }

  ngOnInit() {
    this.marked = !this.selectedRow.toMe;
    this.getAllClients();
    this.getAllSuppliers();

  }

  getAllClients() {
    this.clientService.getAllClients().subscribe(res => {
      this.clients = res;
    });
  }

  getAllSuppliers() {
    this.supplierService.getAllSuppliers().subscribe(res => {
      this.suppliers = res;
    });
  }

  createOrder() {
    const transaction = {
      id: this.selectedRow.id,
      note: this.selectedRow.note,
      toMe: this.selectedRow.toMe,
      businesType: this.selectedRow.owner,
      businesId: this.selectedRow.owner == 'CLIENT' ? this.selectedRow.curentClient[0].id : this.selectedRow.curentSupplier[0].id,
      amount: this.selectedRow.amount,
      devise: this.selectedRow.devise
    };
    this.modal.close(transaction);
  }

  changeForme(toMe: boolean) {
    this.selectedRow.toMe = toMe;
  }


  toggleVisibility(e) {
    this.marked = e.target.checked;
  }

  selectCheckBox(checked: boolean) {
    this.selectedRow.toMe = checked;
  }
}
