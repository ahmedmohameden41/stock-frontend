import {Component, OnDestroy, OnInit} from '@angular/core';
import {ColumnApi, GridApi, GridOptions} from 'ag-grid-community';
import {IProduct} from '../product/product.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from '../../services/products.service';
import {GroupeService} from '../../services/groupe.service';
import {StockService} from '../../services/stock.service';
import {AgGridActionsButtonsComponent} from '../../shared/ag-grid-actions-buttons/ag-grid-actions-buttons.component';
import {DetailsInvoiceComponent} from '../adherant/details-invoice/details-invoice.component';
import {CreateTransactionComponent} from './create-transaction/create-transaction.component';
import {TransactionService} from '../../services/transaction.service';
import {SupplierService} from '../../services/supplier.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
  gridOptions: GridOptions;
  frameworkComponents: any;
  rowData;
  gridApi: GridApi;
  columnApi: ColumnApi;
  products: IProduct [] = [];
  factureType: string = 'FactOui';
  clients: any[];
  orders: any[] = [];
  invoiceItems: any[] = [];
  totalAmount: number = 0;
  remarque: string = '';
  curentClient: any;
  invoice: { status: boolean; partialAmount: number; totalAmount: number; note: string; invoiceItems: any[]; client: any } =
    {status: null, partialAmount: 0, totalAmount: 0, note: '', invoiceItems: [], client: null};
  stocks: any[] = [];
  rowSelection;
  status: boolean;
  buttons: any[];
  saveSubscribtion: Promise<void>;
  suppliers: any[];
  subNewTransaction: Subscription;

  constructor(private modalService: NgbModal,
              private productService: ProductService,
              private clientService: GroupeService,
              private stockService: StockService,
              private supplierService: SupplierService,
              private transactionService: TransactionService) {
    this.frameworkComponents = {
      buttonRenderer: AgGridActionsButtonsComponent,
    };
  }

  ngOnInit() {
    this.invoice.partialAmount = 0;
    this.initAgGrid();
    this.getAllProducts();
    this.getAllClients();
    this.getAllStocks();
  }

  initAgGrid() {
    this.gridOptions = <GridOptions> {
      pagination: false,
      rowModelType: 'clientSide',
      enableColResize: true,
      enableSorting: true,
      enableFilter: true,
      defaultColDef: {
        resizable: true
      },
      columnDefs: [
        {
          headerName: 'المعني',
          field: 'owner',
          floatingFilter: true,
          checkboxSelection: true
        },
        {
          headerName: 'التاريخ',
          field: 'dateCreation',
          floatingFilter: true
        },
        {
          headerName: 'الملاحظة',
          field: 'note',
          floatingFilter: true
        },
        {
          headerName: 'الملكية',
          field: 'toMe',
          cellRenderer: params => params?.value ? 'لصالح الموسسة' : 'ليست لصالح الموسسة'
        },
        {
          headerName: 'المبلغ بالأوقية',
          field: 'amount',
          floatingFilter: true
        },
        {
          headerName: 'المبلغ بالدرهم',
          field: 'devise',
          floatingFilter: true
        }
      ]
    };
    this.buttons = [
      {
        title: 'detail',
        icon: 'fa fa-edit',
        fn: () => this.detail(this.gridApi.getSelectedRows()[0]),
        disabled: () => this.gridApi && this.gridApi.getSelectedRows().length !== 1
      },
      {
        title: 'حذف',
        icon: 'fa fa-trash',
        fn: () => this.deleteTransaction(this.gridApi.getSelectedRows()[0].id),
        disabled: () => (this.gridApi && this.gridApi.getSelectedRows().length !== 1),
      }
    ];
    this.rowSelection = 'single';
  }

  onGridReady($event) {
    this.gridApi = $event.api;
    this.columnApi = $event.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  private loadInvoices() {
    this.transactionService.getAllTransactions().subscribe(transactions => {
      this.rowData = transactions.map(tr => {
        return {
          id: tr.id,
          note: tr.note,
          toMe: tr.toMe,
          owner: tr.businesType == 'CLIENT' ? this.clients.find(c => c.id == tr.businesId)?.name :
            this.suppliers.find(c => c.id == tr.businesId)?.name,
          businesId: tr.businesId,
          businesType: tr.businesType,
          amount: tr.amount,
          devise: tr.devise,
          dateCreation: tr.dateCreation
        };
      });
    });
  }

  newTransaction() {
    const modalRef = this.modalService.open(CreateTransactionComponent, {size: 'lg'});
    modalRef.result.then(res => {
      this.subNewTransaction = this.transactionService.saveTransaction(res).subscribe(grp => {
        this.loadInvoices();
      });
    }).catch(error => console.log(error));
  }

  getAllProducts() {
    this.productService.getAllProducts().subscribe(res => {
      this.products = res;
    });
  }


  getAllClients() {
    this.clientService.getAllClients().subscribe(res => {
      this.clients = res;
      this.getAllSuppliers();
    });
  }

  getAllSuppliers() {
    this.supplierService.getAllSuppliers().subscribe(res => {
      this.suppliers = res;
      this.loadInvoices();
    });
  }


  private getAllStocks() {
    this.stockService.getAllStocks().subscribe(res => {
      this.stocks = res;
    });
  }

  detail(invoice) {
    switch (invoice.businesType) {
      case 'CLIENT':
        invoice.curentClient = [this.clients.find(c => c.id == invoice.businesId)];
        invoice.owner = 'CLIENT';
        break;
      default:
        invoice.curentSupplier = [this.suppliers.find(c => c.id == invoice.businesId)];
        invoice.owner = 'SUPPLIER';
        break;
    }

    const modalRef = this.modalService.open(CreateTransactionComponent, {size: 'lg'});
    modalRef.componentInstance.selectedRow = invoice;
    modalRef.result.then(res => {
      this.subNewTransaction = this.transactionService.saveTransaction(res).subscribe(grp => {
        this.loadInvoices();
      });
    }).catch(error => console.log(error));
  }


  private deleteTransaction(id) {
    this.transactionService.deleteTransaction(id).subscribe(res => {
      if (res) {
        this.loadInvoices();
      }
    });
  }

  private getOwner(params: any) {
    switch (params.data.businesType) {
      case 'CLIENT':
        return this.clients.find(c => c.id == params?.data?.businesId)?.name;
        break;
      default:
        return this.suppliers.find(c => c.id == params?.data?.businesId)?.name;
        break;
    }
  }
}
