import {MfForm, TextboxFormControl, TextBoxType} from 'mf-dynamic-form';
import {ICategory} from '../components/product/product.component';
import {TranslateService} from '@ngx-translate/core';
import {Option} from '@angular/cli/models/interface';


export interface FormParams<T> {
  translate: TranslateService;
  options?: { [key: string]: Option[] };
  model?: T;
  categories?: ICategory[];
  attributesState?: string;
  readOnly?: boolean;
}

export const getCategoryFormDefinition = (params: FormParams<ICategory>): MfForm => {
  const {translate, model} = params;
  return <MfForm> {
    debugMode: true,
    steps: [
      {
        sections: [
          {
            controls: [
              new TextboxFormControl({
                key: 'id',
                value: model?.id,
                exportOnly: true,
                required: false,
                visible: false,
                type: TextBoxType.TEXT,
              }),
              new TextboxFormControl({
                key: 'label',
                label: 'الصنف',
                value: model?.label,
                type: TextBoxType.TEXT
              })
            ]
          },
        ]
      },
    ]
  };
};
