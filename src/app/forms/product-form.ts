import {DropdownFormControl, MfForm, TextboxFormControl, TextBoxType} from 'mf-dynamic-form';
import {ICategory, IProduct} from '../components/product/product.component';
import {TranslateService} from '@ngx-translate/core';
import {Option} from '@angular/cli/models/interface';


export interface FormParams<T> {
  translate: TranslateService;
  options?: { [key: string]: Option[] };
  model?: T;
  categories?: ICategory[];
  attributesState?: string;
  readOnly?: boolean;
}


export const categoryToArray = (categories: ICategory[]) => {
  return categories
    .map((cat, i) => ({ordinal: i, value: cat.id, label: cat.label}));
};

export const getProductFormDefinition = (params: FormParams<IProduct>): MfForm => {
  const {translate, model, categories} = params;
  return <MfForm> {
    debugMode: true,
    steps: [
      {
        sections: [
          {
            controls: [
              new TextboxFormControl({
                key: 'id',
                value: model?.id,
                exportOnly: true,
                required: false,
                visible: false,
                type: TextBoxType.TEXT,
              }),
              new TextboxFormControl({
                key: 'label',
                label: 'الاسم',
                value: model?.label,
                type: TextBoxType.TEXT
              }),
              new DropdownFormControl({
                key: 'category',
                label: 'الاسم',
                type: TextBoxType.TEXT,
                options: categoryToArray(categories),
                value: model?.category?.id,
              })
            ]
          },
        ]
      },
    ]
  };
};
